'use strict';

// Declare app level module which depends on views, and core components
angular.module('myApp', [
  'ngRoute',
  'myApp.version',
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider
    .when('/posts', {
      templateUrl: 'pages/posts/posts.html',
      controller: 'PostsCtrl'
    })
    .when('/markdown-viewer', {
      templateUrl: 'pages/markdownViewer/markdownViewer.html',
      controller: 'MarkdownViewerCtrl'
    })
    .when('/post/:id', {
      templateUrl: 'post/post.html',
      controller: 'PostCtrl'
    })
    .when('/postForm', {
      templateUrl: 'postForm/postForm.html',
      controller: 'PostFormCtrl'
    })
    .otherwise({redirectTo: '/posts'});
}]);
