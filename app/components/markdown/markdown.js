'use strict';

angular.module('myApp')

.directive('markdown', function() {
  var converter = new showdown.Converter();
  return {
    restrict: 'E',
    link: function(scope, element, attrs) {
      var htmlText = converter.makeHtml(element.text());
      element.html(htmlText);
    }
  }
});