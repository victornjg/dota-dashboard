'use strict';

angular.module('myApp')

.directive('postCard', function() {
  return {
    restrict: 'E',
    templateUrl: '/components/postCard/postCard.html'
  }
})