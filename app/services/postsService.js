'use strict';

angular.module('myApp')

.factory('postsFactory', ['$http', function($http) {
  function getPosts() {
    return $http.get('https://jsonplaceholder.typicode.com/posts');
  }
  
  function getPostById(id) {
    return $http.get('https://jsonplaceholder.typicode.com/posts/'.concat(id));
  }
  
  function createPost(post) {
    const config = {
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    }
    return $http.post('https://jsonplaceholder.typicode.com/posts', JSON.stringify(post), config);
  }
  
  return {
    getPosts: getPosts,
    getPostById: getPostById,
    createPost: createPost
  };
}]);

// .service('postsService', postsService);

// /** @ngInject */
// function postsService($http) {
//   var self = this;

//   self.getPosts = function() {
//     return $http.get('https://jsonplaceholder.typicode.com/posts');
//   }

//   self.getPostById = function(id) {
//     return $http.get('https://jsonplaceholder.typicode.com/posts/'.concat(id));
//   }

//   self.createPost = function(post) {
//     const config = {
//       headers: {
//         "Content-type": "application/json; charset=UTF-8"
//       }
//     }
//     return $http.post('https://jsonplaceholder.typicode.com/posts', JSON.stringify(post), config);
//   }
// }