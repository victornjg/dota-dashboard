angular.module('myApp')

.factory('UsersService', ['$http', function($http) {
  function getUsers() {
    return $http.get('https://jsonplaceholder.typicode.com/users');
  }

  return {
    getUsers: getUsers
  }
}]);