'use strict';

describe('PostCtrl', function() {
  beforeEach(module('myApp'));

  var scope, controller, postsFactory, $controller, $rootScope, $q, $routeParams;

  var getPostByIdResult = {
    "userId": 1,
    "id": 1,
    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
  };

  beforeEach(inject(function($injector, _$controller_, _$rootScope_, _$q_, _$routeParams_, _postsFactory_) {
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    $routeParams = _$routeParams_;
    $q = _$q_;

    postsFactory = $injector.get('postsFactory');
    scope = $rootScope.$new();
    controller = $controller('PostCtrl', {
      $scope: scope, postsFactory: postsFactory
    });

    spyOn(postsFactory, 'getPostById').and.returnValue($q.resolve(getPostByIdResult));
  }));

  afterEach(function () {
    postsFactory.getPostById.calls.reset();
  });

  it('should be defined', inject(function() {
    expect(controller).toBeDefined();
  }));

  it('should fetch the data related to the post id received by route param', inject(function() {
    postsFactory.getPostById(1).then(function(result) {
      expect(result).toEqual(getPostByIdResult);
      done();
    });
  }));
});