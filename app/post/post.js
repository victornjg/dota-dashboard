'use strict';

angular.module('myApp')

.controller('PostCtrl', ['$scope', '$routeParams', 'postsFactory', function PostCtrl($scope, $routeParams, postsFactory) {
  var id = $routeParams.id;
  $scope.post = {};

  postsFactory.getPostById(id).then(
    function(response) {
      $scope.post = response.data;
    }, function(error) {
      // TODO: present error alert
    }
  );
}]);