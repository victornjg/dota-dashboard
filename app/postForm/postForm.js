angular.module('myApp')

.controller('PostFormCtrl', ['$scope', '$location', 'UsersService', 'postsFactory', function($scope, $location, UsersService, postsFactory) {
  $scope.post = {};
  $scope.users = [];

  UsersService.getUsers().then(
    function(response) {
      $scope.users = response.data;
    }, function(error) {
      // TODO: present error alert
    }
  );

  $scope.savePost = function() {
    postsFactory.createPost($scope.post).then(
      function(response) {
        // TODO: present success alert
        $location.path('/posts');
      }, function(error) {
        // TODO: present error alert
      }
    );
  };
}]);