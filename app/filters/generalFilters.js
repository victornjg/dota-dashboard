'use strict';

angular.module('myApp')

.filter('previewFilter', function() {
  return function(text) {
    var delimiter = 30;
    if (text.length > delimiter) {
      return text.substring(0, delimiter).concat('...');
    }
    return text;
  };
});
