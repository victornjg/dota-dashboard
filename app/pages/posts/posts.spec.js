'use strict';

describe('PostsCtrl', function () {
  beforeEach(module('myApp'));

  var scope, controller, postsFactory, $controller, $rootScope, $q;
  var getPostsResult = [
    {
      "userId": 1,
      "id": 1,
      "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
      "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
    },
    {
      "userId": 1,
      "id": 2,
      "title": "qui est esse",
      "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
    }
  ];

  beforeEach(inject(function ($injector, _$controller_, _$rootScope_, _$q_, _postsFactory_) {
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    $q = _$q_;

    postsFactory = $injector.get('postsFactory');
    scope = $rootScope.$new();
    controller = $controller('PostsCtrl', {
      $scope: scope, postsFactory: postsFactory
    });

    spyOn(postsFactory, 'getPosts').and.returnValue($q.resolve(getPostsResult));
  }));

  afterEach(function () {
    postsFactory.getPosts.calls.reset();
  });

  it('should be defined', inject(function() {
    expect(controller).toBeDefined();
  }));

  it('should fetch the posts data', function() {
    postsFactory.getPosts().then(function(result) {
      expect(result).toEqual(getPostsResult);
      done();
    });
  });

  describe('$scope.openPost', function () {
    var $location;

    beforeEach(inject(function (_$location_) {
      $location = _$location_;
    }))

    it('should change location path if id is not null or not undefined to "/post/1"', inject(function () {
      scope.openPost(1);
      expect($location.path()).toEqual('/post/1');
    }));

    it('should not change location path if id is null or undefined', inject(function () {
      scope.openPost(null);
      expect($location.path()).toEqual('');
    }));

    it('should change location path to "/postForm"', inject(function () {
      scope.openPostForm();
      expect($location.path()).toEqual('/postForm');
    }));
  })
});