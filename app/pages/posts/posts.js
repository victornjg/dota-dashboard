'use strict';

angular.module('myApp')

.controller('PostsCtrl', ['$scope', '$location', 'postsFactory', function PostsCtrl($scope, $location, postsFactory) {
  $scope.posts = [];
  postsFactory.getPosts().then(
    function(response) {
      $scope.posts = response.data;
    }, function(error) {
      // TODO: present error alert
    }
  );

  $scope.openPost = function(id) {
    if (id === null || id === undefined) return;
    var url = '/post/'.concat(id);
    $location.path(url);
  };

  $scope.openPostForm = function() {
    $location.path('/postForm');
  };
}]);