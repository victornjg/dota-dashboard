'use strict';

describe('MarkdownViewerCtrl', function () {
  beforeEach(module('myApp'));

  it('should be defined', inject(function ($controller) {
    var markdownViewerCtrl = $controller('MarkdownViewerCtrl');
    expect(markdownViewerCtrl).toBeDefined();
  }));
});